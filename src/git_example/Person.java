package git_example;

public class Person {
	private String name, surname;
	private int age;
	
	public Person(String name, String surname){
		this.setName(name);
		this.setSurname(surname);
		
	}
	public Person(String name, String surname, int age){
		this(name,surname);
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	public void setAge(int age) {
		if(age > 0)
			this.age = age;
	}
	public int getAge(){
		return age;
	}
	
	@Override
	public String toString(){
		return name+" "+surname+" "+age;
	}
	public static void main(String[] args){
		
	}

}
